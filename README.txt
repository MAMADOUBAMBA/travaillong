===
=== IFT-4902/7902 PROGRAMMATION AVEC R POUR L'ANALYSE DE DONNÉES
===
=== TRAVAIL LONGITUDINAL
===

CONTENU DE L'ARCHIVE

La présente archive contient les fichiers suivants:

- 'travail-longitudinal.pdf', le texte du mandat.

- 'analyse-critique-conception.txt' et 'analyse-critique-realisation.txt',
  les gabarits pour les analyses critiques des phases «Conception et
  microfonctionnalités» et «Réalisation et clôture».

- 'stations.rds' et 'bixi.rds', deux fichiers contenant des modèles de
  jeux de données BIXI uniformes tels que produits par les fonctions
  'importStations' et 'importData', dans l'ordre.

- 'importData.R', une ébauche de la fonction 'importData' réalisée par
  une analyste de BIXI Montréal.

- 'tests-conception.R', des tests unitaires pour la phase «Conception
  et microfonctionnalités».

- 'tests-realisation.R', des tests unitaires pour la phase
  «Réalisation et clôture». Les tests requièrent des données externes;
  voir ci-dessous.

- 'travail-longitudinal-revenus.xlsx', un exemple de calcul des
  revenus avec un tableur pour les données du fichier 'bixi.rds'.

- 'validateconf-conception', 'validateconf-prototype' et
  'validateconf-realisation', les fichiers de configuration de l'outil
  de validation du système Roger pour le cours IFT-4902. Consulter le
  «Guide de validation avec Roger» pour les instructions d'utilisation
  de l'outil: https://roger-project.gitlab.io/docs/validation.

- 'validateconf-conception-7902', 'validateconf-prototype-7902' et
  'validateconf-realisation-7902', les fichiers de configuration pour
  le cours IFT-7902.

- 'LICENSE', le texte de la licence CC BY-SA sous laquelle sont publiés
  l'énoncé du travail, les fichiers de configuration et les fichiers de
  script R.

DONNÉES EXTERNES

La réalisation du mandat et la validation des livrables à l'aide des
tests unitaires requiert les données externes suivantes:

- les versions uniformisées des données ouvertes de BIXI Montréal du
  projet «Données ouvertes BIXI uniformes»
  (https://vigou3.gitlab.io/bixi-donnees-ouvertes-uniformes);

- les données BIXI uniformes en format RDS importables directement
  dans R du projet 'bixi-donnees-uniformes-test'
  (https://projets.fsg.ulaval.ca/git/projects/VG/repos/bixi-donnees-uniformes-test).
  Des instructions de récupération de ces données se trouvent en
  entête du fichier de tests unitaires 'tests-realisation.R'.
